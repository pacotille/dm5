# Devoir Maison de math n 5 #

C'est un programme en Ocaml. [Les sources sont ici](https://bitbucket.org/pacotille/dm5/src/4846b6c24f6f8698d4e41c1da35385d775ef0d11/dm5.ml?at=master&fileviewer=file-view-default).

Utilisation :

~~~
────────┬──────────────────────────────────────────────────────────────┬────────
        │ Welcome to utop version 1.18.1 (using OCaml version 4.01.0)! │        
        └──────────────────────────────────────────────────────────────┘        

Type #utop_help for help about using utop.

─( 15:08:02 )─< command 0 >──────────────────────────────────────{ counter: 0 }─
utop # #use "dm5.ml";;
val calcul :
    int -> int -> int -> int -> int -> int -> int -> int -> int -> bool = <fun>
val distribute : 'a -> 'a list -> 'a list list = <fun>
val permutation : 'a list -> 'a list list = <fun>
val test_it : int list -> bool = <fun>
val find_it : int list list -> unit = <fun>
val lnum : int list = [0; 1; 2; 4; 5; 6; 7; 8; 9]
val dm5_devinette_2 : int list list -> unit = <fun>
─( 15:08:02 )─< command 1 >──────────────────────────────────────{ counter: 0 }─
utop # List.length (permutation lnum);;
- : int = 362880 
─( 15:08:12 )─< command 2 >──────────────────────────────────────{ counter: 0 }─
utop # dm5_devinette_2 (permutation lnum);;
 5409 
X 142
---------------------                                                           
768078
TrouveFin- : unit = ()
─( 15:08:38 )─< command 3 >──────────────────────────────────────{ counter: 0 }─
utop # 
~~~

