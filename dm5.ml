(*
 * Dm 5
 *)


let calcul q n i c x s e r t =
 (c*1000+i*100+n*10+q) * (s*100+i*10+x) == (t*100000+r*10000+e*1000+n*100+t*10+e) 

let distribute c l =
  let rec insert acc1 acc2 = function
    | [] -> acc2
    | hd::tl ->
      insert (hd::acc1) ((List.rev_append acc1 (hd::c::tl)) :: acc2) tl
  in 
  insert [] [c::l] l

let rec permutation = function
  | [] -> [[]]
  | hd::tl -> 
    List.fold_left (fun acc x -> List.rev_append (distribute hd x) acc) [] (permutation tl)

let test_it l = 
let q = List.hd l in
let n = List.hd (List.tl l) in
let i = List.hd (List.tl (List.tl l)) in
let c = List.hd (List.tl (List.tl (List.tl l))) in
let x = List.hd (List.tl (List.tl (List.tl (List.tl l)))) in
let s = List.hd (List.tl (List.tl (List.tl (List.tl (List.tl l))))) in
let e = List.hd (List.tl (List.tl (List.tl (List.tl (List.tl (List.tl l)))))) in
let r = List.hd (List.tl (List.tl (List.tl (List.tl (List.tl (List.tl (List.tl l))))))) in
let t = List.hd (List.tl (List.tl (List.tl (List.tl (List.tl (List.tl (List.tl (List.tl l)))))))) in
  if calcul q n i c x s e r t then begin 
	print_string " ";
	print_int c; 
	print_int i; 
	print_int n; 
	print_int q; 
print_newline ();
	print_string "X ";
	print_int s; 
	print_int i; 
	print_int x; 
print_newline ();
	print_string "---------------------";
print_newline ();
	print_int t; 
	print_int r; 
	print_int e; 
	print_int n; 
	print_int t; 
	print_int e; 
print_newline ();
	true 
	end else false;; 

let rec find_it = function
  | [] -> print_string "Fin" 
  | x :: l -> if test_it x then  print_string "Trouve" ;find_it l


let lnum = [0;1;2;4;5;6;7;8;9];;
let dm5_devinette_2  l =   find_it l
